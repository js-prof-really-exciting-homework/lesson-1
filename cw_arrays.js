/*
  Задание:
  1. При помощи методов изложеных в arrays.js , переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов.
  2. Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту.
      + Бонусный бал. Вывести на страничку списком
  3. Реализация функции поиска по массиву ITEA_COURSES.
      + Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск.

*/

const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];

//1. Переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов
    // const mapArray = item => item.length
    // let mappedArray = ITEA_COURSES.map(mapArray);
    // console.log(mappedArray);

// 2. Отфильтровать массив ITEA_COURSES по алфавиту и вывести списком в DOM.
    // document.addEventListener('DOMContentLoaded', () => {
    // function compare(a, b){
    //   return a - b;
    // }    
    // let sortedArray = ITEA_COURSES.sort(compare);    
    // let result = `<div>
    //                 <ul>
    //                   ${sortedArray.map(item => `<li>${item}</li>`).join('')}
    //                 </ul>
    //               </div>
    //             `;
    // let div = document.createElement("div");
    //   div.innerHTML = result;
    // document.body.appendChild(div);
    // });

// 3. Реализация функции поиска по массиву ITEA_COURSES.
		const template = `<input type="text" id="searchField"><button id="button">Search</button>
		<div>
		<ul>
		${ITEA_COURSES.map(item => `<li>${item}</li>`).join('')}
		</ul>
		</div>`;
		const div = document.createElement('div');
		div.innerHTML = template;
		document.body.appendChild(div);

		let btn = document.getElementById('button');
		const input = document.getElementById('searchField');

		btn.addEventListener('click', () => {
			const request = input.value.toLowerCase();
			let result = ITEA_COURSES.filter(elem => {
				return elem.toLowerCase().indexOf(request) !== -1;
			})
			console.log(result);
		})
