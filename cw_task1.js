/*

  Данные: http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
  Задача.

  1.  Получить данные и в виде простой таблички вывести список компаний. Для начала используем поля:
      Company | Balance | Registered | Показать адресс | Кол-во employers | показать сотрудников

  2.  Сделать сортировку таблицы по количеству сотрудников и балансу. Сортировка должна происходить по клику
      на заголовок столбца

  3.  По клику на показать адресс должна собиратся строка из полей адресса и показываться на экран.

  4.  По клику на показать сотрудников должна показываться другая табличка формата:
      <- Назад к списку компаний | *Название компании*
      Сотрудники:
      Name | Gender | Age | Contacts

  5.  В второй табличке долен быть реализован поиск сотрудников по их имени, а так же сортировка по
      полу и возрасту.

  Примечание: Весь код должен писатся с учетом синтаксиса и возмжность ES6.

*/

const wrapper = document.getElementById("wrapper");
const table = document.createElement("table");


async function getCompanies(){
  const res = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
  const companies = await res.json();



  const FindNumber = ( string ) =>  Number( string.replace(/\D/, '') )
  function startBalsort(e){
    e.preventDefault();
    companies.sort( (z , c) => {      
      return FindNumber(z.balance) - FindNumber(c.balance);
    });
    table.innerHTML = "";
    table.remove();
    Render();
  }




  function startNameSort(){
    companies.sort( (a , b) => {
        if(a.company < b.company) { return -1; }
        if(a.company > b.company) { return 1; }
        return 0;
    });

    table.innerHTML = "";
    table.remove();
    Render();
  }

  function Render(){
    const title = document.createElement("tr");
     
    title.style.backgroundColor = 'lightgrey';
    title.innerHTML =
      `
      <td id="sortName">Company</td>
      <td id="sortBalance">Balance</td>
      <td>Registered</td>
      <td>Address</td>
      <td>Num. of employees</td>
      <td>Show employees</td>
      `
    wrapper.appendChild(table);
    table.appendChild(title);

    let sortBalanceBtn = document.getElementById('sortBalance');
        sortBalanceBtn.addEventListener('click', startBalsort);
    let sortNameBtn = document.getElementById('sortName');
        sortNameBtn.addEventListener('click', startNameSort);

    companies.map((company, i) => {
      const tr = document.createElement("tr");      
      let {city, zip, country, state, street, house} = company.address;
      
      tr.innerHTML =
      `
        <td>${company.company}</td>
        <td>${company.balance}</td>
        <td data-dateSelector='${i}'><button data-id='${i}' id='btnDate'>Click!</button></td>
        <td data-selector='${i}'><button data-id='${i}' id='btnAddr'>Click!</button></td>
        <td>${company.employers.length}</td>
        <td data-selectorEmpl='${i}'><button data-id='${i}' id='btnShowEmpl'>Show!</button></td>
      `
      table.appendChild(tr);

      let btnAddr = tr.querySelector('#btnAddr');   
          btnAddr.addEventListener('click', (e) => {      
            const td = document.querySelector("[data-selector='" + e.target.dataset.id + "']");
            td.innerHTML = `${country}, ${state}, ${city}, ${zip}, ${street}, ${house}`
          });

      let btnDate = tr.querySelector('#btnDate');   
          btnDate.addEventListener('click', (e) => {      
            const td = document.querySelector("[data-dateSelector='" + e.target.dataset.id + "']");
            td.innerHTML = `${company.registered}`
          });

      const btnShowEmpl = tr.querySelector('#btnShowEmpl');        
            btnShowEmpl.addEventListener('click', (e) => {
              const trHeading = document.createElement('tr');
              const tableEmpl = document.createElement('table');
              const goBack = document.createElement('a');      
              trHeading.innerHTML = `<td>Name</td>
              <td>Gender</td>
              <td>Age</td>
              <td>Contacts</td>`

              trHeading.style.backgroundColor = 'lightgrey';
              goBack.innerText = '<<<--To the previous table';
              goBack.href = '#';

              wrapper.innerHTML = "";
              table.remove();
              wrapper.appendChild(goBack);
              wrapper.appendChild(tableEmpl);
              tableEmpl.appendChild(trHeading);

              company.employers.forEach((employee, i) => {
                const tr = document.createElement('tr');
                console.log(employee);
                let [email1, email2] = employee.emails
                let [phone1, phone2, phone3] = employee.phones
                tr.innerHTML =
                `<td>${employee.name}</td>
                <td>${employee.gender}</td>
                <td>${employee.age} y.o.</td>
                <td>
                <b>E-mails:</b><br />
                ${email1}<br />
                ${email2}<br />
                <b>Phones:</b><br />
                ${phone1}<br />
                ${phone2}<br />
                ${phone3}<br />            
                </td>
                `
                tableEmpl.appendChild(tr);
              });

        goBack.addEventListener('click', (e) => {
          e.preventDefault();
          wrapper.innerHTML = "";        
          wrapper.appendChild(table);
        })
      })
    })
  }
  Render()
}

getCompanies();

